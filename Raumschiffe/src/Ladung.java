
public class Ladung {
	private int anzahl;
	private String name;

	public Ladung(int anzahl, String name) {
		super();
		this.anzahl = anzahl;
		this.name = name;
	}

	public Ladung() {
		super();
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

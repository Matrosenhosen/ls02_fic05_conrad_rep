import java.util.ArrayList;

public class Raumschiff {
	private String name;
	private int energieversorgung, schutzschild, huelle, photonentorpedos, repAndroiden;
	private static ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladung;
	private Kapitaen kapitaen;

	public Raumschiff() {

	}

	/**
	 * @param name Name desRaumschiffes
	 * @param energieversorgung Energieversorgung des Raumschiffes in Prozent
	 * @param schutzschild Schutzschild des Raumschiffes in Prozent
	 * @param huelle Huellenintegritaet des Raumschiffes in Prozent
	 * @param photonentorpedos Anzahl der geladenen Photonentorpedos
	 * @param repAndroiden Anzahl der Reparaturandroiden an Board
	 * @param ladung Die geladene Ladung als ArrayList
	 * @param kapitaen der Kapitaen des Schiffes
	 */
	public Raumschiff(String name, int energieversorgung, int schutzschild, int huelle, int photonentorpedos,
			int repAndroiden, ArrayList<Ladung> ladung, Kapitaen kapitaen) {
		super();
		this.setName(name);
		this.setEnergieversorgung(energieversorgung);
		this.schutzschild = schutzschild;
		this.huelle = huelle;
		this.photonentorpedos = photonentorpedos;
		this.repAndroiden = repAndroiden;
		this.ladung = ladung;
		this.kapitaen = kapitaen;
	}

	/**
	 * @return Name des Raumschiffes
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name Name des Raumschiffes
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return Energieversorgung des Raumschiffes in Prozent
	 */
	public int getEnergieversorgung() {
		return energieversorgung;
	}

	/**
	 * @param energieversorgung Energieversorgung des Raumschiffes in Prozent
	 */
	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}

	/**
	 * @return Schutzschild des Raumschiffes in Prozent
	 */
	public int getSchutzschild() {
		return schutzschild;
	}

	/**
	 * @param schutzschild Schutzschild des Raumschiffes in Prozent
	 */
	public void setSchutzschild(int schutzschild) {
		this.schutzschild = schutzschild;
	}

	/**
	 * @return Huellenintegritaet des Raumschiffes in Prozent
	 */
	public int getHuelle() {
		return huelle;
	}

	/**
	 * @param huelle Huellenintegrität des Raumschiffes in Prozent
	 */
	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}

	/**
	 * @return Anzahl der geladenen Photonentorpedos
	 */
	public int getPhotonentorpedos() {
		return photonentorpedos;
	}

	/**
	 * @param photonentorpedos Anzahl der geladenen Photonentorpedos
	 */
	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}

	/**
	 * @return Anzahl der Reparaturandroiden an Board
	 */
	public int getRepAndroiden() {
		return repAndroiden;
	}

	/**
	 * @param repAndroiden Anzahl der Reparaturandroiden an Board
	 */
	public void setRepAndroiden(int repAndroiden) {
		this.repAndroiden = repAndroiden;
	}

	/**
	 * @return Inhalt des Broadcast Kommunikator
	 */
	public static ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	/**
	 * @param broadcastKommunikator Inhalt des Broadcast Kommunikator
	 */
	public static void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}

	/**
	 * @return Die Ladung des Raumschiffes als ArrayList
	 */
	public ArrayList<Ladung> getLadung() {
		return ladung;
	}

	/**
	 * @param ladung Die Ladung des Raumschiffes
	 */
	public void setLadung(ArrayList<Ladung> ladung) {
		this.ladung = ladung;
	}

	/**
	 * @return Das Kapitaen Objekt des Raumschiffes
	 */
	public Kapitaen getKapitaen() {
		return kapitaen;
	}

	/**
	 * @param kapitaen Das Kapitaen Objekt des Raumschiffes
	 */
	public void setKapitaen(Kapitaen kapitaen) {
		this.kapitaen = kapitaen;
	}

	/**
	 * @param ladung Das zu ladene Ladungs Objekt
	 */
	public void addLadung(Ladung ladung) {
		this.ladung.add(ladung);
	}

	/**
	 * @return Geladene Ladung des Raumschiffes als String
	 */
	public String zeigeLadung() {
		StringBuilder sb = new StringBuilder();
		ladung.forEach(i -> {
			sb.append(i.getName() + ": " + i.getAnzahl() + "\n");
		});
		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	/**
	 * @return Attribute des Raumschiffes + Wert als String
	 */
	@Override
	public String toString() {
		return "Raumschiff [name=" + name + ", energieversorgung=" + energieversorgung + ", schutzschild="
				+ schutzschild + ", huelle=" + huelle + ", photonentorpedos=" + photonentorpedos + ", repAndroiden="
				+ repAndroiden + ", ladung=" + zeigeLadung() + "]";
	}

	/**
	 * @param nachricht Nachricht die dem Broadcast Kommunikator hinzugefügt wird
	 */
	public void nachrichtAnAlle(String nachricht) {
		broadcastKommunikator.add(nachricht);
	}

	/**
	 * @param ziel Das getroffene Raumschiff
	 */
	public void trefferVermerken(Raumschiff ziel) {
		System.out.println(ziel.getName() + " wurde getroffen!");
		if (ziel.getSchutzschild() <= 0) {
			if (ziel.getHuelle() <= 0) {
				nachrichtAnAlle("Die Lebenserhaltungssysteme von " + ziel.getName() + " wurden zerstört");
			} else
				ziel.setHuelle(ziel.getHuelle() - 50);
		} else
			ziel.setSchutzschild(getSchutzschild() - 50);
	}

	/**
	 * @param ziel Das Raumschiff, auf das geschossen wird
	 */
	public void photontorpedosFeuern(Raumschiff ziel) {
		if (photonentorpedos > 0) {
			photonentorpedos--;
			nachrichtAnAlle("Photonentorpedos abgeschossen");
			trefferVermerken(ziel);
		} else
			nachrichtAnAlle("-=*Click*=-");
	}

	/**
	 * @param ziel Das Raumschiff, auf das geschossen wird
	 */
	public void phaserFeuern(Raumschiff ziel) {
		if (getEnergieversorgung() > 50) {
			setEnergieversorgung(getEnergieversorgung() - 50);
			nachrichtAnAlle("Phaserkanone abgeschossen!");
			trefferVermerken(ziel);
		} else
			nachrichtAnAlle("-=*Click*=-");
	}

	/**
	 * @return gibt den Broadcastkommunikator zurück
	 */
	public String getLogbuch() {
		StringBuilder sb = new StringBuilder();
		getBroadcastKommunikator().forEach(i -> {
			sb.append(i + "\n");
		});
		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	/**
	 * @param anzahl Die anzahl der Nachzuladenen Photonentorpedos
	 */
	public void nachladen(int anzahl) {
		for (int i = 0; i < ladung.size(); i++) {
			if (ladung.get(i).getName().equals("Photonentorpedos")) {
				if (ladung.get(i).getAnzahl() < anzahl) {
					this.setPhotonentorpedos(getPhotonentorpedos() + anzahl);
					ladung.get(i).setAnzahl(ladung.get(i).getAnzahl() - anzahl);
					System.out.println(anzahl + " Photonentorpedo(s) eingesetzt");
				} else {
					System.out.println(ladung.get(i).getAnzahl() + "Photonentorpedo(s) eingesetzt");
					this.setPhotonentorpedos(getPhotonentorpedos() + ladung.get(i).getAnzahl());
					ladung.get(i).setAnzahl(0);
				}
			} else {
				System.out.println("Keine Photonentorpedos gefunden!");
				nachrichtAnAlle("-=*Click*=-");
			}
		}
	}

	/**
	 * 
	 */
	public void ladungAufraeumen() {
		for (int i = 0; i < ladung.size(); i++) {
			if (ladung.get(i).getAnzahl() <= 0)
				ladung.remove(i);
		}
		ladung.trimToSize();
	}

	/**
	 * @param huelle Huelle reparieren Ja/Nein
	 * @param energieversorgung Energieversorgung reparieren Ja/Nein
	 * @param schild Schild Reparieren Ja/Nein
	 * @param androiden Anzahl der Einzusetzenden Reparaturandroiden Ja/Nein
	 */
	public void reparieren(boolean huelle, boolean energieversorgung, boolean schild, int androiden) {
		if (androiden > this.getRepAndroiden()) {
			androiden = getRepAndroiden();
		}
		int n = 0;
		if (huelle)
			n++;

		if (energieversorgung)
			n++;

		if (schild)
			n++;

		int reparieren = (int) (((((Math.random() * 100)) + 1) * androiden) / n);

		if (huelle)
			setHuelle(getHuelle() + reparieren);
		
		if (energieversorgung)
			setEnergieversorgung(getEnergieversorgung() + reparieren);
		
		if (schild)
			setSchutzschild(getSchutzschild() + reparieren);
	}
}
